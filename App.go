package main

import (
	"io"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/gin-gonic/gin"
)

type App struct {
	Addr      string
	Engine    *gin.Engine
	Client    *http.Client
	Config    AppConfig
	AssetsDir string
}

type AppConfig struct {
	AccessControlAllowHeaders string            `json:"accessControlAllowHeaders"`
	HeaderReplaceMap          map[string]string `json:"headerReplaceMap"`
	HeaderReplaceValueMap     map[string]string `json:"headerReplaceValueMap"`
}

type Dummy struct{}

var dummy Dummy

type Set[T comparable] map[T]Dummy

func (set *Set[T]) Add(values ...T) {
	for _, v := range values {
		(*set)[v] = dummy
	}
}

func (set *Set[T]) Del(values ...T) {
	for _, v := range values {
		delete(*set, v)
	}
}

var proxyPrefix = "/proxy/"

func (app *App) SetAccessControlAllowHeaders(headers ...string) {

	headersSet := make(Set[string])

	headersSet.Add(headers...)

	headerStr := ""

	for header := range headersSet {
		headerStr += header + ","
	}
	app.Config.AccessControlAllowHeaders = strings.TrimSuffix(headerStr, ",")
}

// AddHeaderReplace 设置Header的替换关系，old替换为new，同时删除old
func (app *App) AddHeaderReplace(old string, new string) {
	app.Config.HeaderReplaceMap[old] = new
}

func (app *App) SetHeaderDefaultValue(key string, value string) {
	app.Config.HeaderReplaceValueMap[key] = value
}

func (app *App) AddHeaderReplaceWithDefault(old string, new string, defaultVale string) {
	app.AddHeaderReplace(old, new)
	app.SetHeaderDefaultValue(old, defaultVale)
}

func (app *App) AddHeaderReplaceArr(replaceArr [][2]string) {
	for _, replaceItem := range replaceArr {
		app.Config.HeaderReplaceMap[replaceItem[0]] = replaceItem[1]
	}
}

func (app *App) ClearHeaderReplace() {
	for k := range app.Config.HeaderReplaceMap {
		delete(app.Config.HeaderReplaceMap, k)
	}
}

func (app *App) handleMethodFunc(c *gin.Context) {

	requri := strings.TrimPrefix(c.Request.RequestURI, proxyPrefix)

	req, _ := http.NewRequest(c.Request.Method, requri, c.Request.Body)

	req.Header = c.Request.Header.Clone()

	for header, replacedHeader := range app.Config.HeaderReplaceMap {
		value := req.Header.Get(header)
		if value == "" {
			if v, ok := app.Config.HeaderReplaceValueMap[header]; ok {
				value = v
			}
		}
		req.Header.Set(replacedHeader, value)
		req.Header.Del(header)
	}

	res, e := app.Client.Do(req)

	if e != nil {

		c.String(http.StatusInternalServerError, e.Error())
		c.Abort()

		return
	}

	c.Status(res.StatusCode)
	xcookies := ""
	for _, v := range res.Cookies() {
		c.SetCookie(v.Name, v.Value, v.MaxAge, v.Path, v.Domain, v.Secure, v.HttpOnly)
		xcookies += v.Name + "=" + v.Value + ";"
	}
	c.Header("x-cookie", xcookies)
	for k, v := range res.Header {
		if k == "Set-Cookie" || strings.ToLower(k) == "set-cookie" {
			continue
		}
		for _, vi := range v {
			c.Header(k, vi)
		}
	}

	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", app.Config.AccessControlAllowHeaders)
	c.Header("Access-Control-Expose-Headers", "*")

	io.Copy(c.Writer, res.Body)

	c.Writer.Flush()
	defer res.Body.Close()

}

func initProxyHandle() {
	app.Engine.GET(proxyPrefix+"*w", app.handleMethodFunc)
	app.Engine.POST(proxyPrefix+"*w", app.handleMethodFunc)
	app.Engine.HEAD(proxyPrefix+"*w", app.handleMethodFunc)
}

func initCors() {

	app.Engine.Use(func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
	})

	app.Engine.OPTIONS("/*w", func(c *gin.Context) {

		c.Status(http.StatusNoContent)
		if app.Config.AccessControlAllowHeaders != "" {
			c.Header("Access-Control-Allow-Headers", app.Config.AccessControlAllowHeaders)
		} else {
			c.Header("Access-Control-Allow-Headers", c.Request.Header.Get("Access-Control-Request-Headers"))
		}

	})

}

func initConfigHandle() {
	app.Engine.POST("/api/config", func(c *gin.Context) {

		headers := c.PostFormArray("AccessControlAllowHeaders")
		app.SetAccessControlAllowHeaders(headers...)

		replaceHeaders := c.PostFormArray("HeaderReplace")
		if len(replaceHeaders) > 0 {

			app.ClearHeaderReplace()
			for _, headerReplace := range replaceHeaders {
				headerArr := strings.SplitN(headerReplace, ":", 3)
				if len(headerArr) == 2 {
					app.AddHeaderReplace(headerArr[0], headerArr[1])
				} else if len(headerArr) == 3 {
					app.AddHeaderReplaceWithDefault(headerArr[0], headerArr[1], headerArr[2])
				} else {
					c.Status(http.StatusBadRequest)
					return
				}
			}
		}

		c.Status(http.StatusNoContent)

	})

	app.Engine.GET("/api/config", func(c *gin.Context) {

		c.JSON(http.StatusOK, app.Config)
	})
}

func initStatic() {

	d, err := os.Open(app.AssetsDir)
	if err != nil {
		return
	}
	d.Close()

	fileServer := http.FileServer(http.Dir(app.AssetsDir))
	app.Engine.NoRoute(func(c *gin.Context) {
		f, err := os.Open(path.Join(app.AssetsDir, strings.TrimLeft(c.Request.URL.EscapedPath(), "/")))
		if err != nil {
			c.File(path.Join(app.AssetsDir, "index.html"))
			return
		}
		f.Close()
		fileServer.ServeHTTP(c.Writer, c.Request)
	})

}

func (app *App) Run() {

	initProxyHandle()
	initConfigHandle()
	initCors()
	initStatic()

	app.Engine.Run(app.Addr)
}
