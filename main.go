package main

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gin-gonic/gin"
	"golang.org/x/net/http2"
)

var app App

var DefaultAddr = "127.0.0.22:8080"

var DefaultAssetsDir = "./assets"

var DefaultConfigName = "setting.json"

var SignalChan = make(chan os.Signal)

func main() {
	signal.Notify(SignalChan, syscall.SIGINT)

	go func() {
		<-SignalChan

		SaveConfig()
		os.Exit(0)
	}()

	run()
}

func run() {

	initCmdFlags()

	initHTTPClient()

	initGin()

	loadConfig()

	app.Run()
}

func initCmdFlags() {

	hoststr := flag.String("bind", DefaultAddr, "ip:port")
	assetsDir := flag.String("path", DefaultAssetsDir, "default assets path")

	flag.Parse()

	app.Addr = *hoststr
	app.AssetsDir = *assetsDir
}

func initGin() {

	r := gin.New()

	ginmode := os.Getenv(gin.EnvGinMode)
	if ginmode == gin.ReleaseMode {
		r.Use(gin.Recovery())
	} else {
		r.Use(gin.Logger(), gin.Recovery())
	}

	r.SetTrustedProxies(nil)

	app.Engine = r
}

func initHTTPClient() {

	var tr = &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}

	http2.ConfigureTransport(tr)

	var client = &http.Client{
		Transport: tr,
	}

	app.Client = client
}

func loadConfig() {
	appConfig := AppConfig{
		HeaderReplaceMap:      make(map[string]string),
		HeaderReplaceValueMap: make(map[string]string),
	}
	if fileContent, err := os.ReadFile(DefaultConfigName); err == nil {
		e := json.Unmarshal(fileContent, &appConfig)
		if e != nil {
			fmt.Errorf("%v", e)
		}

	}
	app.Config = appConfig

}

func SaveConfig() {
	if data, err := json.Marshal(app.Config); err == nil {

		os.WriteFile(DefaultConfigName, data, 0644)
	}
}
